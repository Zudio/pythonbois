# Shopkeeper
#
# This short demo highlights how keyboard input can be used.
# Just run this file and respond to the prompts.
#
# When we enter text at the keyboard it is returned as a character
# string.  If we want to know its value as a Python expression
# (if any) we must "evaluate" it.
#
# For the purposes of this demonstration we use a relatively
# uncommon Python data structure, a "dictionary".  It is similar
# to the lists you are already familiar with, but instead of being
# indexed using integers, dictionaries can be indexed by values of
# any type.  In this case we use the dictionary to associate
# character strings with floating point numbers.
#

# A list of the products we stock
inventory = ['bread', 'milk', 'ice cream', 'lemonade', 'chocolate']
# A dictionary of prices, indexed by the names of the products
price = {'bread'     : 3.55,
         'milk'      : 1.00,
         'ice cream' : 4.75,
         'lemonade'  : 2.25,
         'chocolate' : 6.80
        }

# Here we use the text entered at the keyboard as a string
print("Hello, I'm your friendly local shopkeeper.")
product = input("What would you like? ")

if not (product in inventory):
    print("I'm sorry, we don't stock that!")
else:
    # Here we assume the user will enter a whole number
    how_many = input("And how many would you like? ")
    quantity = eval(how_many) # evaluate the string to get an integer (we hope!)
    cost = quantity * price[product]  
    print("That will be $" + str(round(cost, 2)) + "!")

print("Thank you for your patronage!")

